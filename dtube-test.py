#!/usr/bin/python
# -*- coding:Utf-8 -*-

#   Dtube test
#   Copyright (C) 2009 Olivier Le Thanh Duong, Guillaume Desmottes

#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 2 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import logging
logging.basicConfig(level=logging.DEBUG)

import dbus
from dbus.service import method, signal, Object
import gobject

from connection_watcher import ConnectionWatcher

from telepathy.interfaces import CHANNEL_TYPE_CONTACT_LIST, CONNECTION_INTERFACE_REQUESTS,\
    CONNECTION_INTERFACE_SIMPLE_PRESENCE, CHANNEL_INTERFACE_GROUP, CHANNEL_INTERFACE
from telepathy.constants import HANDLE_TYPE_LIST, HANDLE_TYPE_CONTACT, CONNECTION_PRESENCE_TYPE_AVAILABLE,\
    CONNECTION_PRESENCE_TYPE_AWAY, CONNECTION_PRESENCE_TYPE_EXTENDED_AWAY, CONNECTION_PRESENCE_TYPE_BUSY,\
    TUBE_STATE_OPEN
from telepathy.client.channel import Channel
from telepathy.client.conn import Connection


TUBE_SERVICE = "be.staz.DTubetest"
dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)

# TODO: import when tube API is stable
CHANNEL_INTERFACE_TUBE = CHANNEL_INTERFACE + ".Interface.Tube.DRAFT"
CHANNEL_TYPE_DBUS_TUBE = CHANNEL_INTERFACE + ".Type.DBusTube.DRAFT"

TUBE_CHANNEL_STATE_LOCAL_PENDING = 0
TUBE_CHANNEL_STATE_REMOTE_PENDING = 1
TUBE_CHANNEL_STATE_OPEN = 2
TUBE_CHANNEL_STATE_NOT_OFFERED = 3

# List of presences types we consider as online
ONLINE_TYPES = [CONNECTION_PRESENCE_TYPE_AVAILABLE,
                CONNECTION_PRESENCE_TYPE_AWAY,
                CONNECTION_PRESENCE_TYPE_EXTENDED_AWAY,
                CONNECTION_PRESENCE_TYPE_BUSY]

def search_contact_from_id(contact, conn):
    """Return the handle that match the id in given connexion"""

    if CONNECTION_INTERFACE_REQUESTS not in conn:
        logging.warning("CONNECTION_INTERFACE_REQUESTS not implemented in %s" % (conn.service_name))
        return None

    # get contacts list
    yours, path, props = conn[CONNECTION_INTERFACE_REQUESTS].EnsureChannel({
        'org.freedesktop.Telepathy.Channel.ChannelType': CHANNEL_TYPE_CONTACT_LIST,
        'org.freedesktop.Telepathy.Channel.TargetHandleType': HANDLE_TYPE_LIST,
        'org.freedesktop.Telepathy.Channel.TargetID': 'subscribe'})

    chan = Channel(conn.service_name, path)
    contacts = chan[CHANNEL_INTERFACE_GROUP].GetMembers()
    ids = conn.InspectHandles(HANDLE_TYPE_CONTACT, contacts)

    for handle, id in zip(contacts, ids):
        if id == contact:
            return handle

def contact_is_online(handle, conn):
    """Is contact online?"""
    presences = conn[CONNECTION_INTERFACE_SIMPLE_PRESENCE].GetPresences([handle])
    type, status, msg = presences[handle]
    return type in ONLINE_TYPES

def offer_tube(conn, handle, tube_service):
    """Offer a dtube to contact designed by @handle"""
    # FIXME: check if contact supports our tube
    yours, path, props = conn[CONNECTION_INTERFACE_REQUESTS].EnsureChannel({
        'org.freedesktop.Telepathy.Channel.ChannelType': CHANNEL_TYPE_DBUS_TUBE,
        'org.freedesktop.Telepathy.Channel.TargetHandleType': HANDLE_TYPE_CONTACT,
        'org.freedesktop.Telepathy.Channel.TargetHandle': handle,
        CHANNEL_TYPE_DBUS_TUBE + '.ServiceName': tube_service})

    def tube_state_changed_cb(state):
        if state == TUBE_STATE_OPEN:
            print "Tube accepted "

            tube_conn = dbus.connection.Connection(address)
            # Create and export the object
            obj = ExempleObject(tube_conn, "/Example")

        elif state == TUBE_CHANNEL_STATE_REMOTE_PENDING:
            print "Other player is invited, waiting for response..."
        else:
            print "Tube state changed", state

    def tube_closed_cb():
        print "Tube closed"

    chan = Channel(conn.service_name, path)
    chan[CHANNEL_INTERFACE_TUBE].connect_to_signal('TubeChannelStateChanged', tube_state_changed_cb)
    chan[CHANNEL_INTERFACE].connect_to_signal('Closed', tube_closed_cb)
    print "Invited contact on %s" % tube_service
    address = chan[CHANNEL_TYPE_DBUS_TUBE].OfferDBusTube({})

def offer_tube_to_contact_cb(watcher, conn, contact, tube_service):
    """When a new connection is added, check if @contact is present on and online on it, if found offer him a tube of @tube_service"""
    #TODO : Remove itself when contact is found.
    handle = search_contact_from_id(contact, conn)
    if handle: 
        print "Contact %s found in %s" % (contact, conn.service_name)
        if contact_is_online(handle, conn):
            print "contact online"
            offer_tube(conn, handle, tube_service)
    else:
        print "Contact %s not found in %s" % (contact, conn.service_name)

class Handler(Object):
    """Create an Handler service which wait for a connection"""
    @dbus.service.method("org.gnome.Empathy.TubeHandler",
            in_signature='soouu', out_signature='')
    def HandleTube(self, bus_name, connection, channel, handle_type, handle):
        #We need a try here cause strangly it doesn't display exceptions
        try:
            logging.info("Handling a new tube!")
            # TODO: ask to the user if he wants to accept or decline the tube
            conn = Connection(bus_name, connection)
            chan = Channel(bus_name, channel)

            address = chan[CHANNEL_TYPE_DBUS_TUBE].AcceptDBusTube()
            tube_conn = dbus.connection.Connection(address)
            # FIXME This should be in a callback
            # Get the object from the remote end of the tube
            obj = tube_conn.get_object(object_path="/Example")
            
            # Call a methode on it
            ret = obj.HelloWorld("Hello world")
            print ret

        except Exception, e:
            print e
            raise

def create_handler(tube_service):
    """create a D-Bus handler service,
    which wait for someone to offer us a tube"""
    logging.info("Creating the handler service")
    # Open the local session bus
    bus = dbus.SessionBus()
    handler_name = "org.gnome.Empathy.DTubeHandler.%s" % tube_service
    handler_path = "/org/gnome/Empathy/DTubeHandler/%s" % tube_service.replace('.','/')
    logging.info("Bus name : %s" % handler_name)
    logging.info("Bus path : %s" % handler_path)
    name = dbus.service.BusName(handler_name, bus)
    handler = Handler(bus, handler_path)
    return name, handler

def invite_contact(contact, tube_service):
    watcher = ConnectionWatcher()
    print "Looking for contact..."
    watcher.connect('connection-added', offer_tube_to_contact_cb, contact, tube_service)

class ExempleObject(dbus.service.Object):
    @dbus.service.method("com.example.SampleInterface",
                         in_signature='s', out_signature='as')
    def HelloWorld(self, hello_message):
        print (str(hello_message))
        return ["Hello", " from example-service.py"]

if __name__ == "__main__":

    if sys.argv[1] == '--handler':
        # We must keep a pointer on those otherwhise they get destroyed
        name, handler = create_handler(TUBE_SERVICE)
    else:
        contact = sys.argv[1]
        invite_contact(contact, TUBE_SERVICE)

    loop = gobject.MainLoop()
    loop.run()

# vim:set shiftwidth=4 tabstop=4 expandtab textwidth=79:
